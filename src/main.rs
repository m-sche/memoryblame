#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]

use sysinfo::{System, SystemExt, ProcessExt, PidExt, ProcessRefreshKind};
use std::path::Path;
use std::io::{stdout, Write, BufRead, BufReader};
use std::process::{Command, Stdio};
use std::time::{SystemTime, Duration};
use std::collections::{VecDeque, HashMap};
use rusqlite::{named_params, Connection, Result};
use users::get_user_by_uid;

const DB: &str = "memoryblame.db";  // SQLite file
const MIN_MEMORY: u64 = 100_000;    // Minimal memory in kB for process to be remembered

const SLEEP_SEC: u64 = 60;          // Sleep between memory usage sampling, must be 1m
const RETRY_SLEEP_SEC: u64 = 5;     // Sleep between DB retres when DB is busy
const RETRY_N: u32 = 3;             // How many ties to try before continuing another sampling cycle

const AGG_SEC: u64 = 60 * 20;       // After this much time or later will the aggregation procedure run
const AGG_10M_AFTER: &str = "4 hours";
const AGG_1H_AFTER: &str = "3 days";
const AGG_SYSTEM_AFTER: &str = "3 days";

fn fmt_process_name(process: &sysinfo::Process) -> Option<String> {
    if process.cmd().len() > 0 {
        let cmd = Path::new(process.cmd()[0].as_str()).file_name()?.to_str()?;
        Some(format!("{} {}", cmd, process.cmd()[1..].join(" ")))
    } else {
        Some(process.name().to_string())
    }
}

fn is_db_busy(res: Result<usize, rusqlite::Error>) -> bool {
    match res {
        Ok(_) => false,
        Err(err) => match err {
            rusqlite::Error::SqliteFailure(e, msg) => {
                match e.code {
                    rusqlite::ErrorCode::DatabaseBusy | rusqlite::ErrorCode::DatabaseLocked => return true,
                    code => panic!("Some other Sqlite failure, code: {:?}. {:?}", code, msg),
                }
            },
            err => panic!("Some other error: {}", err)
        }
    }
}

enum PersistItem {
    System {
        timestamp: u64,
        used_mem: u64,
        available_mem: u64,
        sum_rss: u64,
    },
    ProcessMeta {
        start_time: u64,
        pid: u32,
        ppid: u32,
        user: String,
        cmd: String,
    },
    Metric {
        start_time: u64,
        pid: u32,
        timestamp: u64,
        rss: u64,           // in KB
        pss: Option<u64>,   // in KB
    },
}

struct PersistUnit<'l> {
    stmt: rusqlite::Statement<'l>,
    items: VecDeque<Box<PersistItem>>,
}

impl<'l> PersistUnit<'l> {
    pub fn new<'a>(conn: &'a rusqlite::Connection, sql: &str) -> PersistUnit<'a> {
        PersistUnit {
            stmt: conn.prepare(sql).unwrap(),
            items: VecDeque::new(),
        }
    }

    fn push(&mut self, item: PersistItem) {
        self.items.push_back(Box::new(item));
    }

    fn persist(&mut self) -> Result<(), rusqlite::Error> {
        let mut retry = 0;

        while let Some(item) = self.items.pop_front() {
            let res = match *item {
                PersistItem::System{timestamp, used_mem, available_mem, sum_rss} => {
                    self.stmt.execute(named_params! {
                        ":timestamp": timestamp,
                        ":used_mem": used_mem,
                        ":available_mem": available_mem,
                        ":sum_rss": sum_rss,
                    })
                },
                PersistItem::ProcessMeta{start_time, pid, ppid, user, cmd} => {
                    self.stmt.execute(named_params! {
                        ":start_time": start_time,
                        ":pid": pid,
                        ":ppid": ppid,
                        ":user": user,
                        ":cmd": cmd,
                    })
                },
                PersistItem::Metric{start_time, pid, timestamp, rss, pss} => {
                    self.stmt.execute(named_params! {
                        ":start_time": start_time,
                        ":pid": pid,
                        ":timestamp": timestamp,
                        ":rss": rss,
                        ":pss": pss,
                    })
                },
            };

            if is_db_busy(res) {
                print!("!");
                stdout().flush().unwrap();
                retry += 1;

                if retry < RETRY_N {
                    std::thread::sleep(Duration::from_secs(RETRY_SLEEP_SEC));
                } else {
                    break;
                }
            }
        }
        Ok(())
    }
}

fn render_agg_sql(table_source: &str, table_target: &str, agg_after: &str, agg_period: u32, static_columns: Vec<&str>, metric_columns: Vec<&str>, source_is_agg: bool) -> String {
    let mut metric_columns_agg = metric_columns.iter().map(|col| format!("\
        cast(avg({col}) as int) as avg_{col}, \
        cast(max({col}) as int) as max_{col}\
    ", col=col)).collect::<Vec<String>>().join(", ");

    let mut metric_columns_str = metric_columns.join(", ");

    if source_is_agg {
        metric_columns_agg = metric_columns.iter().map(|col| format!("\
            cast(avg(avg_{col}) as int) as avg_{col}, \
            cast(max(max_{col}) as int) as max_{col}\
        ", col=col)).collect::<Vec<String>>().join(", ");

        metric_columns_str = metric_columns
            .iter()
            .map(|col| format!("avg_{col}, max_{col}", col=col))
            .collect::<Vec<String>>()
            .join(", ")
    }

    let groupby = (1..static_columns.len()+2).map(|n| n.to_string()).collect::<Vec<String>>().join(", ");

    format!("
        begin;

        with
        rounded as (
            select
                {static_columns}
                datetime(timestamp / {agg_period} * {agg_period}, 'unixepoch') as timestamp,
                {metric_columns_str}
            from {table_source}
        ),
        max_timestamp as (
            select max(timestamp) as max_ts
            from rounded
        )
        insert into {table_target}
        select
            {static_columns}
            strftime('%s', timestamp) as timestamp,
            {metric_columns_agg}
        from rounded
        join max_timestamp
        where timestamp < datetime(max_ts, '-{agg_after}', '-{agg_period} seconds')
        group by {groupby};

        with
        max_timestamp as (
            select datetime(max(timestamp / {agg_period} * {agg_period}), 'unixepoch') as max_ts
            from {table_source}
        )
        delete
        from {table_source}
        where datetime(timestamp, 'unixepoch') < (
            select datetime(max_ts, '-{agg_after}', '-{agg_period} seconds')
            from max_timestamp
        );

        commit;
        ",
        static_columns=static_columns.join(", ") + (if static_columns.len() > 0 {","} else {""}),
    )
}

fn aggregate_metrics(conn: &rusqlite::Connection) -> Result<()> {
    print!("a");
    stdout().flush().unwrap();

    let agg_sql = render_agg_sql(
        "metric",                   // table_source
        "metric_agg_10m",           // table_target
        AGG_10M_AFTER,              // agg_after
        600,                        // agg_period
        vec!["start_time", "pid"],  // static_columns
        vec!["rss", "pss"],         // metric_columns
        false,                      // source_is_agg
    );
    conn.execute_batch(agg_sql.as_str()).expect("Error in 10m aggregation sql");

    let agg_sql = render_agg_sql(
        "metric_agg_10m",           // table_source
        "metric_agg_1h",            // table_target
        AGG_1H_AFTER,               // agg_after
        3600,                       // agg_period
        vec!["start_time", "pid"],  // static_columns
        vec!["rss", "pss"],         // metric_columns
        true,                       // source_is_agg
    );
    conn.execute_batch(agg_sql.as_str()).expect("Error in 1h aggregation sql");

    let agg_sql = render_agg_sql(
        "system",                   // table_source
        "system_agg_1h",            // table_target
        AGG_SYSTEM_AFTER,           // agg_after
        3600,                       // agg_period
        vec![],                     // static_columns
        vec!["used_mem", "available_mem", "sum_rss"],  // metric_columns
        false,                      // source_is_agg
    );
    conn.execute_batch(agg_sql.as_str()).expect("Error in system aggregation sql");

    Ok(())
}

fn main() -> Result<()> {
    let mut sys = System::new_all();
    let conn = Connection::open(DB).expect("Cannot open connection");

    conn.pragma_update(None, "auto_vacuum", "full")?;

    conn.execute("
        create table if not exists process (
            start_time integer,
            pid integer,
            ppid integer,
            user text,
            cmd text,
            primary key (start_time, pid)
        )
    ", []).expect("Cannot create \"process\" table");

    conn.execute("
        create table if not exists metric (
            start_time integer,
            pid integer,
            timestamp integer,
            rss integer,
            pss integer,
            primary key (start_time, pid, timestamp)
            foreign key (start_time, pid) references process(start_time, pid)
        );
    ", []).expect("Cannot create \"metric\" table");

    for table in ["metric_agg_10m", "metric_agg_1h"] {
        conn.execute(format!("
            create table if not exists {table} (
                start_time integer,
                pid integer,
                timestamp integer,
                avg_rss integer,
                max_rss integer,
                avg_pss integer,
                max_pss integer,
                primary key (start_time, pid, timestamp)
                foreign key (start_time, pid) references process(start_time, pid)
            );
        ").as_str(), []).expect(format!("Cannot create \"{table}\" table").as_str());
    }

    conn.execute("
        create view if not exists
            metric_avg (start_time, pid, timestamp, memory, rate, set_size) as
        select start_time, pid, timestamp, coalesce(pss, rss) as memory, '1m' as rate,
            case when pss is null then 'rss' else 'pss' end as set_size
        from metric
        union
        select start_time, pid, timestamp, coalesce(avg_pss, avg_rss) as memory, '10m' as rate,
            case when avg_pss is null then 'rss' else 'pss' end as set_size
        from metric_agg_10m
        union
        select start_time, pid, timestamp, coalesce(avg_pss, avg_rss) as memory, '1h' as rate,
            case when avg_pss is null then 'rss' else 'pss' end as set_size
        from metric_agg_1h
    ", []).expect("Cannot create \"metric_avg\" view");

    conn.execute("
        create table if not exists system (
            timestamp integer,
            used_mem integer,
            available_mem integer,
            sum_rss integer,
            primary key (timestamp)
        )
    ", []).expect("Cannot create \"system\" table");

    conn.execute(format!("
        create table if not exists system_agg_1h (
            timestamp integer,
            avg_used_mem integer,
            max_used_mem integer,
            avg_available_mem integer,
            max_available_mem integer,
            avg_sum_rss integer,
            max_sum_rss integer,
            primary key (timestamp)
        );
    ").as_str(), []).expect(format!("Cannot create \"system_agg_1h\" table").as_str());

    conn.execute("
        create view if not exists
            system_avg (timestamp, used_mem, available_mem, sum_rss, rate) as
        select timestamp, used_mem, available_mem, sum_rss, '1m' as rate
        from system
        union
        select
            timestamp,
            avg_used_mem as used_mem,
            avg_available_mem as available_mem,
            avg_sum_rss as sum_rss,
            '1h' as rate
        from system_agg_1h
    ", []).expect("Cannot create \"system_avg\" view");

    let mut last_aggregation = SystemTime::UNIX_EPOCH;

    let mut persistor = HashMap::from([
        ("process", PersistUnit::new(&conn, "
            insert or ignore into process
            values (:start_time, :pid, :ppid, :user, :cmd)
        ")),
        ("metric", PersistUnit::new(&conn, "
            insert into metric
            values (:start_time, :pid, :timestamp, :rss, :pss)
        ")),
        ("system", PersistUnit::new(&conn, "
            insert into system
            values (:timestamp, :used_mem, :available_mem, :sum_rss)
        ")),
    ]);

    // Determine where to find the pss binary
    #[cfg(debug_assertions)]
    let rustc_env = "debug";
    #[cfg(not(debug_assertions))]
    let rustc_env = "release";

    let mut pss_reader = Command::new(format!{"target/{}/pss", rustc_env})
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start pss reader");
    let pss_stdin = pss_reader.stdin.as_mut().unwrap();
    let mut pss_stdout = BufReader::new(pss_reader.stdout.as_mut().unwrap());
    let mut pss_line = String::new();

    loop {
        let refresh_time = (SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap()).as_secs();

        print!("[");
        stdout().flush().unwrap();

        sys.refresh_all();

        let mut sum_rss = 0;

        for (pid, process) in sys.processes() {
            sum_rss += process.memory();
            if process.memory() > MIN_MEMORY {
                // Try getting pss
                pss_stdin.write(format!("{}\n", pid.as_u32()).as_bytes()).unwrap();
                pss_line.clear();
                let pss = match pss_stdout.read_line(&mut pss_line) {
                    Ok(_) => pss_line.trim().parse::<u64>().map_or(None, |v| {
                        if v > 0 {Some(v)} else {None}
                    }),
                    Err(_) => None,
                };

                let p = PersistItem::ProcessMeta {
                    start_time: process.start_time(),
                    pid: pid.as_u32(),
                    ppid: process.parent().unwrap().as_u32(),
                    user: get_user_by_uid(process.uid).unwrap().name().to_str().unwrap().to_string(),
                    cmd: fmt_process_name(process).unwrap()
                };
                let m = PersistItem::Metric {
                    start_time: process.start_time(),
                    pid: pid.as_u32(),
                    timestamp: refresh_time,
                    rss: process.memory(),
                    pss,
                };

                persistor.get_mut("process").unwrap().push(p);
                persistor.get_mut("metric").unwrap().push(m);
            }
        }

        persistor.get_mut("system").unwrap().push(PersistItem::System {
            timestamp: refresh_time,
            used_mem: sys.used_memory(),
            available_mem: sys.available_memory(),
            sum_rss,
        });

        for (_, unit) in &mut persistor {
            unit.persist()?;
        }

        let now = SystemTime::now();
        if now.duration_since(last_aggregation).unwrap() > Duration::from_secs(AGG_SEC) {
            aggregate_metrics(&conn)?;
            last_aggregation = now;
        }

        print!("]");
        stdout().flush().unwrap();

        let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
        if SLEEP_SEC as i64 - now as i64 + refresh_time as i64 > 0 {
            std::thread::sleep(Duration::from_secs(SLEEP_SEC + refresh_time - now));
        }
    }

    //Ok(())
}
