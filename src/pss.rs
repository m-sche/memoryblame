use lazy_static::lazy_static;
use regex::Regex;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn read_process(pid: u32) -> u64 {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^Pss:\s*(\d+) kB$").unwrap();
    }
    let file = match File::open(format!("/proc/{pid}/smaps_rollup")) {
        Ok(file) => file,
        Err(_) => return 0
    };

    for l in BufReader::new(file).lines() {
        if let Ok(line) = l {
            if line.starts_with("Pss:") {
                if let Some(caps) = RE.captures(&line) {
                    if let Some(size) = caps.get(1) {
                        return size.as_str().parse::<u64>().unwrap_or(0);
                    }
                }
            }
        } else {
            return 0
        }
    }
    0
}

fn main() {
    let mut input = String::new();
    loop {
        let pss = std::io::stdin().read_line(&mut input).map_or(0, |_| {
            input.trim().parse::<u32>().map_or(0, |pid| read_process(pid))
        });
        println!("{}", pss);
        input.clear();
    }
}
