# Memoryblame

This tool started as a project that would help me determine who of my colleagues is using the most memory on our shared data science server. You can use it for the exact same thing or for monitoring the memory usage of your own individual processes over time.

## Dependencies

This is a Rust program so you need `rust` and `cargo` installed. The program consists of two binaries, the main `memoryblame` and a separate `pss` binary that is a very simple program which communicates over standard IO with the main program and can read PSS memory usage of processes for which it has enough privileges. The reason for separation of the two is that you can set setuid bit to the `pss` binary and make it owned by root to be able to read PSS usage of all processes. If PSS cannot be determined, RSS is measured.

Run the main program using `cargo run --release --bin memoryblame`.

To visualize the results, you can use attached Jupyter notebook or read the SQLite database yourself and visualize however you want.

## How it works

Each 60 seconds, RSS memory usage of all running processes is determined. Processes with RSS usage above 100 MB are stored in a SQLite database. The information stored is:
- process start time
- process pid
- process exe (comandline arguments)
- RSS memory usage
- PSS memory usage if `pss` process has necessary permissions

In addition, system-wide statistics are collected:
- sum of RSS of all processes (including those smaller than 100 MB)
- total used memory
- total available memory

Each 20 minutes, a check is performed and if there's enough data collected (based on constants defined on the top of `src/main.rs`), the measurements are aggregated (averaged over 10 minutes and later over 1 hour) to save storage space.

## How PSS is collected

PSS (proportional set size) is a more useful memory usage method than RSS for the purpose of blaming people over memory usage. For memory pages which are shared among multiple (say $`n`$) processes, only $`1/n`$ are attributed to the process.

To get PSS, the `pss` binary utilizes `/proc/*/smaps_rollup` special files. These files can be read only by the owner of the process.

The protocol `pss` uses to communicate with outer world (mainly the `memoryblame` binary, but it can be used as a standalone program as well) is dead simple: It takes PIDs on its standard input delimited by newline characters and after reading each line, it outputs a number representing the PSS usage in kB on its stdout. If any error occurs (insufficient permissions, nonexisting process, invalid PID format...), the output is `0`. The program runs in an infinite loop.
